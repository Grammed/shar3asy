package fun.madeby.shar3azy.repository;


import fun.madeby.shar3azy.models.ShareClub;
import lombok.Data;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.jupiter.api.Assertions.*;
@ActiveProfiles("test")

@DataJpaTest
class UserRepositoryTest {
	@Autowired
	UserRepository repository;

@Test
void findUsersByshareClubId() {
}

@Test
void findUsersByShareClub_IdOrderByRolesAsc() {
}

@Test
void findByUsername() {
}

@Test
void existsByUsername() {
}

@Test
void existsByEmail() {
}

@Test
void count() {
	Long count = repository.count();

	assert(count == 0);
}
}