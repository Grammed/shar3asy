package fun.madeby.shar3azy.repository;


import fun.madeby.shar3azy.controllers.AuthController;
import fun.madeby.shar3azy.models.ShareClub;
import fun.madeby.shar3azy.security.WebSecurityConfig;
import lombok.Data;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.jupiter.api.Assertions.*;
@ActiveProfiles("test")
// https://www.baeldung.com/spring-componentscan-filter-type
@ComponentScan(basePackages = {"fun.madeby.shar3azy"},
excludeFilters = {@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
	classes = {WebSecurityConfig.class})})
@DataJpaTest
class ShareClubRepositoryTest {

	@Autowired
	ShareClubRepository repository;

@Test
void findByName() {
}

@Test
@DisplayName("(findById[ShareClub]) pass id for BS created ShareClub -> Return it")
void findById() {
	ShareClub returnedShareClub = repository.getShareClubById(1L);
	System.out.println(returnedShareClub.getName());

	assert(returnedShareClub.getName().equalsIgnoreCase("BootStrap origin default ShareClub name"));

}



}