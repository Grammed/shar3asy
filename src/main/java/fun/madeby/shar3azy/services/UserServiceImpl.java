package fun.madeby.shar3azy.services;

import fun.madeby.shar3azy.models.User;
import fun.madeby.shar3azy.repository.UserRepository;
import fun.madeby.shar3azy.security.jwt.JwtUtils;
import lombok.RequiredArgsConstructor;
import org.hibernate.ObjectNotFoundException;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService{
private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

private final UserRepository USER_REPO;


@Override
public List<User> retrieveAllUsersInClub(Long userId) {
	User user = getUserById(userId);
	return USER_REPO.findUsersByshareClubId(user.getShareClub().getId());
}

@Override
public Optional<User> getUserByName(String username) {
	return USER_REPO.findByUsername(username);
}

@Override
public User getUserById(Long id) {
	return USER_REPO.findById(id).orElseThrow(RuntimeException::new);
}

@Override
public Boolean usernameTaken(String username) {
	return USER_REPO.existsByUsername(username);
}

@Override
public Boolean emailTaken(String email) {
	return USER_REPO.existsByEmail(email);
}

@Override
public void addSingleUser(User user) {
	USER_REPO.save(user);
}

@Override
public long numOfUsers() {
	return USER_REPO.count();
}

@Override
public List<User> retrieveAllUsers() {
	return USER_REPO.findAll() ;
}

}
