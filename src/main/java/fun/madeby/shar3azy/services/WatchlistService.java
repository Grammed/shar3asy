package fun.madeby.shar3azy.services;

import fun.madeby.shar3azy.models.Watchlist;

import java.util.Optional;

public interface WatchlistService {
	Optional<Watchlist> getWatchlistByName(String name);
	Optional<Watchlist> getWatchlistById (Long id);
	Watchlist addSingleWatchlist(Watchlist watchlist);
}
