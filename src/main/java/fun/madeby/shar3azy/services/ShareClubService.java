package fun.madeby.shar3azy.services;

import fun.madeby.shar3azy.models.ShareClub;

import java.util.Optional;

public interface ShareClubService {
	ShareClub getShareClubByName(String name);
	ShareClub getShareClubById(Long id);
	ShareClub addSingleShareClub(ShareClub shareClub);
}
