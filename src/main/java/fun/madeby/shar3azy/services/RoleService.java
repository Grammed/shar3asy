package fun.madeby.shar3azy.services;

import fun.madeby.shar3azy.models.ERole;
import fun.madeby.shar3azy.models.Role;
import java.util.Optional;

public interface RoleService {
	Optional<Role> getRoleByName(ERole name);
	Optional<Role> getRoleById(Long id);
	Role addSingleRole(Role role);
	long numOfRoles();

}
