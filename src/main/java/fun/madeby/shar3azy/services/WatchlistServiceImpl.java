package fun.madeby.shar3azy.services;

import fun.madeby.shar3azy.models.Watchlist;
import fun.madeby.shar3azy.repository.WatchlistRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class WatchlistServiceImpl implements WatchlistService{
	private final WatchlistRepository WATCH_LIST_REPO;

@Override
public Optional<Watchlist> getWatchlistByName(String name) {
	return WATCH_LIST_REPO.findWatchlistByName(name);
}

@Override
public Optional<Watchlist> getWatchlistById(Long id) {
	return WATCH_LIST_REPO.findById(id);
	//return WATCH_LIST_REPO.findWatchlistById(id);
}

@Override
public Watchlist addSingleWatchlist(Watchlist watchlist) {
	return WATCH_LIST_REPO.save(watchlist);
}
}
