package fun.madeby.shar3azy.services;

import fun.madeby.shar3azy.models.User;
import java.util.List;
import java.util.Optional;

public interface UserService {
	List<User> retrieveAllUsersInClub(Long shareClubId);
	Optional<User> getUserByName(String username);
	User getUserById(Long id);
	Boolean usernameTaken(String username);
	Boolean emailTaken(String email);
   void addSingleUser(User user);
   long numOfUsers();
   List<User> retrieveAllUsers();

}
