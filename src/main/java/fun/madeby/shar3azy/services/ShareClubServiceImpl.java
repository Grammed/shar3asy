package fun.madeby.shar3azy.services;

import fun.madeby.shar3azy.models.ShareClub;
import fun.madeby.shar3azy.repository.ShareClubRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ShareClubServiceImpl implements ShareClubService{
	private final ShareClubRepository SHARE_CLUB_REPO;

@Override
public ShareClub getShareClubByName(String name) {
	return SHARE_CLUB_REPO.findByName(name);
}

@Override
public ShareClub getShareClubById(Long id) {
	return SHARE_CLUB_REPO.getShareClubById(id);
}

@Override
public ShareClub addSingleShareClub(ShareClub shareClub) {
	return SHARE_CLUB_REPO.save(shareClub);
}
}
