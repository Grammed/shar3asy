package fun.madeby.shar3azy.services;

import fun.madeby.shar3azy.models.PortfolioItem;

import java.util.Optional;

public interface PortfolioService {
	Optional<PortfolioItem> getPortfolioItemByName (String name);
	Optional<PortfolioItem> getPortfolioItemById (Long id);
	PortfolioItem addSinglePortfolioItem(PortfolioItem portfolioItem);
}
