package fun.madeby.shar3azy.services;

import fun.madeby.shar3azy.models.PortfolioItem;
import fun.madeby.shar3azy.repository.PortfolioRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PortfolioServiceImpl implements PortfolioService {
	private final PortfolioRepository PORTFOLIO_REPO;

@Override
public Optional<PortfolioItem> getPortfolioItemByName(String name) {
	return Optional.empty();
}

@Override
public Optional<PortfolioItem> getPortfolioItemById(Long id) {
	return PORTFOLIO_REPO.findById(id);
}

@Override
public PortfolioItem addSinglePortfolioItem(PortfolioItem portfolioItem) {
	return PORTFOLIO_REPO.save(portfolioItem);
}

}
