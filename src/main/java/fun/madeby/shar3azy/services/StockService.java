package fun.madeby.shar3azy.services;

import fun.madeby.shar3azy.models.Stock;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface StockService {
List<Stock> searchStringInSymbol(String searchString);

ResponseEntity<Map<String, Object>> retrieveAllStocksPaged(String symbol, int page, int size);
Stock getStockBySymbolDotExchange(String symbolDotExchange);
}
