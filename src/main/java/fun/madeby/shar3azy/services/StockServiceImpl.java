package fun.madeby.shar3azy.services;

import fun.madeby.shar3azy.models.Stock;
import fun.madeby.shar3azy.repository.StockRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class StockServiceImpl implements StockService{
	public final StockRepository STOCK_REPO;


@Override
public List<Stock> searchStringInSymbol(String searchString) {
	// using query method so % not required searchString = "%" + searchString.toUpperCase(Locale.ROOT) + "%";
	return STOCK_REPO.findStockBySymbolContainsOrderByName(searchString.toUpperCase(Locale.ROOT));
}

@Override
public ResponseEntity<Map<String, Object>> retrieveAllStocksPaged(String symbol, int page, int size) {
	try{
		List<Stock> stocks;
		Pageable pageDetail = PageRequest.of(page, size);

		Page<Stock> pageStocks;
		if(symbol == null)
			pageStocks = STOCK_REPO.findAll(pageDetail);
		else
		pageStocks = STOCK_REPO.findBySymbolContaining(symbol, pageDetail);

		stocks = pageStocks.getContent();

		Map<String, Object> response = new HashMap<>();
		response.put("stocks", stocks);
		response.put("currentPage", pageStocks.getNumber());
		response.put("totalItems", pageStocks.getTotalElements());
		response.put("totalPages", pageStocks.getTotalPages());
		response.put("hasContent", pageStocks.hasContent());
		response.put("hasPrevious", pageStocks.hasPrevious());
		response.put("hasNext", pageStocks.hasNext());

		return new ResponseEntity<>(response, HttpStatus.OK);
	} catch (Exception e) {
		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}

@Override
public Stock getStockBySymbolDotExchange(String symbolDotExchange) {
	if(!symbolDotExchange.contains(".XLON"))
		symbolDotExchange = symbolDotExchange + ".XLON";

	return STOCK_REPO.findStockBySymbol(symbolDotExchange);
}
}
