package fun.madeby.shar3azy.services;

import fun.madeby.shar3azy.models.ERole;
import fun.madeby.shar3azy.models.Role;
import fun.madeby.shar3azy.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {
	private final RoleRepository ROLE_REPO;


@Override
public Optional<Role> getRoleByName(ERole name) {
	return ROLE_REPO.findByName(name);
}

@Override
public Optional<Role> getRoleById(Long id) {
	return ROLE_REPO.findById(id);
}

@Override
public Role addSingleRole(Role role) {
	return ROLE_REPO.save(role);
}

@Override
public long numOfRoles() {
	return ROLE_REPO.count();
}
}
