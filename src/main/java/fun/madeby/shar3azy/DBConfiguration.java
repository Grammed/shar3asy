package fun.madeby.shar3azy;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ConfigurationProperties("spring.datatsource")
@SuppressWarnings("unused")
public class DBConfiguration {
private String driverClassName;
private String url;
private String username;
private String password;

@Profile("dev")
@Bean
public String devDatabaseConnection() {
	System.out.println("DB connection for DEV - Local_MySQL");
	System.out.println(driverClassName);
	System.out.println(url);
	return "DB connection for DEV - Local_MySQL";
}

@Profile("dev")
@Bean
public String testDatabaseConnection() {
	System.out.println("DB connection to TEST - H2");
	System.out.println(driverClassName);
	System.out.println(url);
	return "DB connection for TEST - H2";
}
@Profile("dev")
@Bean
public String prodDatabaseConnection() {
	System.out.println("DB connection for PROD - Remote_MySQL");
	System.out.println(driverClassName);
	System.out.println(url);
	return "DB connection for DEV - Remote_MySQL";
}


}
