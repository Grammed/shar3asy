package fun.madeby.shar3azy.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@Table(name = "PortfolioItem")
public class PortfolioItem {

	// todo all set to null for now and stocks entered as strings
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Long id;

// Unidirectional, relationship not needed in Stock, not even mentioned there.
@OneToOne
@JoinColumn(name = "stock_Id", referencedColumnName = "id")
private Stock stock;


//@NotBlank
@Column(name = "Notes", columnDefinition = "VARCHAR(500) NULL")
@Size(max = 500)
private String notes;

//@NotBlank
@Column(name = "Owned", columnDefinition = "INT NULL")
private Integer owned;

//@NotBlank
@Column(name = "PriceAtLastTransaction", columnDefinition = "DOUBLE NULL")
//@Size(max = 50)
private Double priceAtLastTransaction;

//@NotBlank
@Column(name="DateAtLastTransaction", columnDefinition = "DATETIME NULL")
@DateTimeFormat(pattern = "yyyy-MM-dd-HH:mm")
private LocalDateTime dateAtLastTransaction;

/* KEEP SIMPLER FOR NOW
//@NotBlank
@Column(name = "AverageGCPS", columnDefinition = "DOUBLE NULL")
//@Size(max = 50)
private Integer averageCostPerShare;

//@NotBlank
@Column(name = "GrossCost", columnDefinition = "DOUBLE NULL")
//@Size(max = 50)
private Integer GrossCostOfShares;
*/


//@NotBlank
@Column(name = "ValueAtLastTransaction", columnDefinition = "DOUBLE NULL")
//@Size(max = 50)
private Double valueAtLastTransaction;

@Column(name = "Active", columnDefinition = "BOOLEAN NOT NULL")
private Boolean active;




/* KEEP SIMPLER FOR NOW, this info will come from transactions and will require service BL
//@NotBlank
@Column(name = "ClubLastPurchased", columnDefinition = "DOUBLE NULL")
//@Size(max = 20)
private Integer clubsLastPurchasedPrice;

//@NotBlank
@Column(name="LastPurchasedDate", columnDefinition = "DATETIME NULL")
@DateTimeFormat(pattern = "yyyy-MM-dd-HH:mm")
private LocalDateTime clubsLastPurchasePriceDate;

//@NotBlank
@Column(name = "ClubLastSold", columnDefinition = "DOUBLE NULL")
//@Size(max = 20)
private Integer clubsLastSoldPrice;

//@NotBlank
@Column(name="LastSoldDate", columnDefinition = "DATETIME NULL")
@DateTimeFormat(pattern = "yyyy-MM-dd-HH:mm")
private LocalDateTime clubsLastSoldPriceDate;*/

// @JoinColumn == owner of relationship == child
//@JsonIgnore
@JsonBackReference
@NotNull
@ManyToOne
@JoinColumn(name = "shareClubId")
private ShareClub shareClub;

public PortfolioItem(Stock stock, String notes, Integer owned,
                     Double priceAtLastTransaction, LocalDateTime dateAtLastTransaction,
                     ShareClub shareClub) {
	this.stock = stock;
	this.notes = notes;
	this.owned = owned;
	this.priceAtLastTransaction = priceAtLastTransaction;
	this.dateAtLastTransaction = dateAtLastTransaction;
	this.valueAtLastTransaction = this.owned * this.priceAtLastTransaction;
	this.shareClub = shareClub;
	this.active = true;
}

public PortfolioItem(Stock stock, ShareClub shareClub) {
	this.stock = stock;
	this.notes = "PortfolioItem note added in the null/empty version constructor.";
	this.owned = null;
	this.priceAtLastTransaction = null;
	this.dateAtLastTransaction = null;
	this.valueAtLastTransaction = null;
	this.shareClub = shareClub;
	this.active = true;

}

public void addPortfolioItemToShareClub(PortfolioItem portfolioItem) {
	if(!shareClub.getPortfolioItems().contains(this))
		shareClub.getPortfolioItems().add(this);
}


}
