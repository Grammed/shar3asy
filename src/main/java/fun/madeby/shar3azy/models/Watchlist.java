package fun.madeby.shar3azy.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@Table(name = "WatchList")
public class Watchlist {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	@Column(name = "Name", columnDefinition = "VARCHAR(50) NOT NULL")
	@Size(max = 50)
	private String name;

	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "shareClubId")
	private ShareClub shareClub;

	public Watchlist(String name, ShareClub shareClub) {
		this.name = name;
		this.shareClub = shareClub;
	}


public void addWatchListToShareClub(Watchlist watchlist) {
	if(!shareClub.getWatchlists().contains(this))
		shareClub.getWatchlists().add(this);
}






}
