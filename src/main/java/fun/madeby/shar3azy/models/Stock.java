package fun.madeby.shar3azy.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@Table(name = "Stock")
public class Stock {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Long id;

@Column(name = "Symbol", columnDefinition = "VARCHAR(30) NOT NULL UNIQUE")
private String symbol;

@Column(name = "Name", columnDefinition = "VARCHAR(200) NOT NULL UNIQUE")
private String name;

@JsonManagedReference
@ManyToOne
@JoinColumn(name = "exchangeId")
private StockExchange exchange;


}
