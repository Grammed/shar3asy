package fun.madeby.shar3azy.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@Table(name = "StockExchage")
public class StockExchange {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer id;

@Column(name = "Name", columnDefinition = "VARCHAR(150) NOT NULL UNIQUE")
private String name;

@Column(name = "ExchangeSymbol", columnDefinition = "VARCHAR(30) NOT NULL UNIQUE")
private String xSymbol;

@JsonBackReference
@OneToMany(mappedBy = "exchange",
	cascade= {CascadeType.PERSIST,
				CascadeType.MERGE,
				CascadeType.DETACH})
private Set<Stock> stocks = new HashSet<>();





}
