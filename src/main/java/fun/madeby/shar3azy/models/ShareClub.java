package fun.madeby.shar3azy.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@Table(name = "ShareClub")
public class ShareClub {

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Long id;

@NotBlank // todo Autopopulate with welcome name of SC.
@Size(max = 100)
@Column(name = "Name", columnDefinition = "VARCHAR(100)")
private String name;

@Column(name = "ExtraOptions", columnDefinition = "BOOLEAN")
private boolean extraOptions;

@JsonBackReference
@OneToMany(mappedBy = "shareClub", cascade = {
	CascadeType.PERSIST,
	CascadeType.MERGE,
	CascadeType.DETACH})
private Set<User> users = new HashSet<>();

@JsonManagedReference
@OneToMany(mappedBy = "shareClub", cascade = {
	CascadeType.PERSIST,
	CascadeType.MERGE,
	CascadeType.DETACH,
	CascadeType.REMOVE})
private Set<Watchlist> watchlists = new HashSet<>();

@JsonManagedReference
@OneToMany(mappedBy = "shareClub", cascade = {
	CascadeType.PERSIST,
	CascadeType.MERGE,
	CascadeType.DETACH,
	CascadeType.REMOVE})
private Set<PortfolioItem> portfolioItems = new HashSet<>();

public ShareClub(String name) {
	this.name = name;
}

}
