package fun.madeby.shar3azy.models;


public enum ERole {
    ROLE_SITE,
    ROLE_MANAGER,
    ROLE_MEMBER
}