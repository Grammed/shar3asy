package fun.madeby.shar3azy.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
@Table(name = "sec_users",
	uniqueConstraints = {
		@UniqueConstraint(columnNames = "username"),
		@UniqueConstraint(columnNames = "email")
	})
public class User {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Long id;
@NotBlank
@Column(name = "Username", columnDefinition =  "VARCHAR(20) NOT NULL")
@Size(max = 20)
private String username;

@NotBlank
@Size(max = 50)
@Email
@Column(name = "Email", columnDefinition =  "VARCHAR(50) NOT NULL")
private String email;

@NotBlank
@Size(max = 120)
@Column(name = "Password", columnDefinition =  "VARCHAR(120) NOT NULL")
private String password;

//@Size(max = 32768) // todo No validator could be found for constraint 'javax.validation.constraints.Size' validating type 'java.lang.Short'. Check configuration for 'streetNumber'] with root cause
@Column(name = "StreetNumber", columnDefinition = "SMALLINT NULL")
private short streetNumber;

@Size(max = 100)
@Column(name = "StreetName", columnDefinition = "VARCHAR(100) NULL")
private String street;

@Size(max = 50)
@Column(name = "Town", columnDefinition = "VARCHAR(50) NULL ")
private String town;

@Size(max = 50)
@Column(name = "Region", columnDefinition = "VARCHAR(50) NULL")
private String region;

@Size(max = 20)
@Column(name = "Zip", columnDefinition = "VARCHAR(20) NULL")
private String zip;

//@JsonIgnore
@JsonManagedReference
@ManyToOne
@JoinColumn(name = "shareClubId")
private ShareClub shareClub;

@ManyToMany(fetch = FetchType.LAZY)
@JoinTable(name = "sec_user_roles",
	joinColumns = @JoinColumn(name = "user_id"),
	inverseJoinColumns = @JoinColumn(name = "role_id"))
private Set<Role> roles = new HashSet<>();

// @todo test User account creation shareClub == null
public User(String username, String email, String expectedEncoded) {
	this.username = username;
	this.email = email;
	this.password = expectedEncoded;
	this.shareClub = null;
}

// @todo BS adds the same (default) shareClub to admin/manager and member
public User(String username, String email, String expectedEncoded, ShareClub shareClub) {
	this.username = username;
	this.email = email;
	this.password = expectedEncoded;
	this.shareClub = shareClub;
}

public void addRole(Role role) {
	this.roles.add(role);
}

// required when user is Optional
public ShareClub getShareClub() {
	return shareClub;
}

@Override
public String toString() {
	return "User{" +
		       "id=" + id +
		       ", username='" + username + '\'' +
		       ", email='" + email + '\'' +
		       ", password='" + password + '\'' +
		       ", roles=" + roles +
		       '}';
}
}