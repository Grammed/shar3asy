package fun.madeby.shar3azy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Shar3azyApplication {

public static void main(String[] args) {
	SpringApplication.run(Shar3azyApplication.class, args);
}

}
