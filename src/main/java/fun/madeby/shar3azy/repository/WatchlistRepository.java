package fun.madeby.shar3azy.repository;

import fun.madeby.shar3azy.models.ShareClub;
import fun.madeby.shar3azy.models.Watchlist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WatchlistRepository extends JpaRepository<Watchlist, Long> {
List<Watchlist> findWatchlistsByshareClubId(Long shareClubId);

List<Watchlist> findWatchlistsByShareClubOrderByNameAsc(ShareClub shareclub);

Optional<Watchlist> findWatchlistByName(String name);

Optional<Watchlist> findWatchlistById(Long id);

}
