package fun.madeby.shar3azy.repository;

import fun.madeby.shar3azy.models.ShareClub;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ShareClubRepository extends JpaRepository<ShareClub, Long> {
 ShareClub findByName(String name);
 ShareClub getShareClubById(Long id);
}
