package fun.madeby.shar3azy.repository;

import fun.madeby.shar3azy.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
   //@Query("SELECT u FROM sec_users u WHERE u.shareClubId = ?1")
    List<User> findUsersByshareClubId(Long shareClubId);
    List<User> findUsersByShareClub_IdOrderByRolesAsc(Long shareClubId);
    Optional<User> findByUsername(String username);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);
}