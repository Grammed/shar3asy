package fun.madeby.shar3azy.repository;

import fun.madeby.shar3azy.models.ERole;
import fun.madeby.shar3azy.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
    Optional<Role> findById(Long id);
}
