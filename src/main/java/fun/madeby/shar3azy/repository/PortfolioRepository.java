package fun.madeby.shar3azy.repository;

import fun.madeby.shar3azy.models.PortfolioItem;
import fun.madeby.shar3azy.models.ShareClub;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PortfolioRepository extends JpaRepository<PortfolioItem, Long> {
List<PortfolioItem> findPortfolioItemsByshareClubId(Long shareClubId);

List<PortfolioItem> findPortfolioItemsByShareClub(ShareClub shareClub);

Optional<PortfolioItem> findPortfolioItemsByStockName(String stockName);

Optional<PortfolioItem> findWatchlistById(Long id);
}
