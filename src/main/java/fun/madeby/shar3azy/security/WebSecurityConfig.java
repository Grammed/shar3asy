package fun.madeby.shar3azy.security;

import fun.madeby.shar3azy.security.jwt.AuthEntryPointJwt;
import fun.madeby.shar3azy.security.jwt.AuthTokenFilter;
import fun.madeby.shar3azy.security.services.UserDetailsServiceImpl;
import lombok.RequiredArgsConstructor;
import org.h2.server.web.WebServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
// import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
// import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@RequiredArgsConstructor
@EnableWebSecurity
@EnableGlobalMethodSecurity(
	// securedEnabled = true,
	// jsr250Enabled = true,
	prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
@Autowired
UserDetailsServiceImpl userDetailsService;

@Autowired
private AuthEntryPointJwt unauthorizedHandler;

@Autowired
private PasswordEncoderConfig passwordEncoderConfig;

@Bean
public AuthTokenFilter authenticationJwtTokenFilter() {
	return new AuthTokenFilter();
}

// # Access to H2 console
@Bean
ServletRegistrationBean h2servletRegistration() {
	ServletRegistrationBean registrationBean = new ServletRegistrationBean(new WebServlet());
	registrationBean.addUrlMappings("/console/*");
	return registrationBean;
}

// GD note to self -> this still uses BCrypt, checkmakers used PasswordEncoder direct in spring.
@Override
public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
	authenticationManagerBuilder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoderConfig.passwordEncoder());
}

@Bean
@Override
public AuthenticationManager authenticationManagerBean() throws Exception {
	return super.authenticationManagerBean();
}


/*@Bean Moved to config class
public PasswordEncoder passwordEncoder() {
	return new BCryptPasswordEncoder();
}*/

@Override
protected void configure(HttpSecurity http) throws Exception {
	http.cors().and().csrf().disable()
		.exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
		.authorizeRequests().antMatchers("/api/auth/**").permitAll()
		.antMatchers("/api/test/**").permitAll()
		.antMatchers("/api/mmav1/**").permitAll()
		.antMatchers("/").permitAll() // #access to all to base url (for h2 console)
		.and().authorizeRequests().antMatchers("/console/**").permitAll() // # and to all console + urls
		//         .antMatchers("/h2/**").hasRole("OWNER")// # could not get this to work was initial version ROLE_ is auto inserted)
		.anyRequest().authenticated();

	http.headers().frameOptions().disable(); // #This needs to be disabled for H2 too

	http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
}
}