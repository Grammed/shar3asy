package fun.madeby.shar3azy.security;

/** GD01082021 Originally in WebSecurity Config which has to be excluded for JPA tests
 * so moved here so it can be accessed by bootstrap. !!This doc lives until!! Bootstrap not used
 * for in memory testing of users (needs access to PasswordEncoder). Note in original checkmakers
 * solution when this was moved a vanilla Spring version of PasswordEncoder was used in web
 * security config.
 */

import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class PasswordEncoderConfig {

	@Bean
	public PasswordEncoder passwordEncoder() { return new BCryptPasswordEncoder(); }
}
