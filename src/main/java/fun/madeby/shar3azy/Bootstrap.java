package fun.madeby.shar3azy;


import fun.madeby.shar3azy.models.*;
import fun.madeby.shar3azy.services.*;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class Bootstrap {
private final UserServiceImpl USER_SERV_IMPL;
private final RoleServiceImpl ROLE_SERV_IMPL;
private final PasswordEncoder P_ENCODER;
private final ShareClubService SHARECLUB_SERV;
private final WatchlistService WATCHLIST_SERV;
private final StockService STOCK_SERV;
//private final Environment env;

@PostConstruct
public void init() {

	// String[] activeProfiles = env.getActiveProfiles();

	//if (Arrays.stream(activeProfiles).anyMatch("test"::equalsIgnoreCase)) {

	if (ROLE_SERV_IMPL.numOfRoles() == 0) {
		Role role1 = new Role(ERole.ROLE_SITE);
		Role role2 = new Role(ERole.ROLE_MANAGER);
		Role role3 = new Role(ERole.ROLE_MEMBER);

		ROLE_SERV_IMPL.addSingleRole(role1);
		ROLE_SERV_IMPL.addSingleRole(role2);
		ROLE_SERV_IMPL.addSingleRole(role3);
	}


	if (USER_SERV_IMPL.numOfUsers() == 0) {

		Stock stock1 = STOCK_SERV.getStockBySymbolDotExchange("TTA");
		Stock stock2 = STOCK_SERV.getStockBySymbolDotExchange("DGED");
		Stock stock3 = STOCK_SERV.getStockBySymbolDotExchange("RDSA");
		Stock stock4 = STOCK_SERV.getStockBySymbolDotExchange("RESB");
		Stock stock5 = STOCK_SERV.getStockBySymbolDotExchange("CASH");
		Stock stock6 = STOCK_SERV.getStockBySymbolDotExchange("CATL");


		ShareClub defaultShareClub = new ShareClub("BootStrap origin default ShareClub name");
		//ShareClub defaultShareClub2 = new ShareClub("BootStrap origin default ShareClub name 2")

		Watchlist defaultWatchlist = new Watchlist("BootStrap origin default name of Watchlist", defaultShareClub);
		Watchlist defaultWatchlist2 = new Watchlist("BootStrap origin default name of Watchlist2", defaultShareClub);


		defaultWatchlist.addWatchListToShareClub(defaultWatchlist);
		defaultWatchlist2.addWatchListToShareClub(defaultWatchlist2);

		/*
		NON NULL CONSTRUCTOR VERSIONS
		LocalDateTime localDateTime = LocalDateTime.of(2012, Month.MAY, 25, 20, 22);
		PortfolioItem allPortfolioItemsDirectInShareClub01 = new PortfolioItem(
			stock1, "Name and symbols just updated to stock object 121, " +
				                                                 "will it work.. have made unidirectional.", 92137, 1.23523461234, LocalDateTime.now(), defaultShareClub);

		PortfolioItem allPortfolioItemsDirectInShareClub02 = new PortfolioItem(
			stock2,"Static Date time passed through. Name and symbols are currently strings, " +
				                                                "these will be from stock object in the future though.", 92137, 1.23523461234, localDateTime, defaultShareClub);
*/

		PortfolioItem allPortfolioItemsDirectInShareClub01 = new PortfolioItem(stock1, defaultShareClub);
		PortfolioItem allPortfolioItemsDirectInShareClub02 = new PortfolioItem(stock2, defaultShareClub);
		allPortfolioItemsDirectInShareClub01.addPortfolioItemToShareClub(allPortfolioItemsDirectInShareClub01);
		allPortfolioItemsDirectInShareClub02.addPortfolioItemToShareClub(allPortfolioItemsDirectInShareClub02);

		SHARECLUB_SERV.addSingleShareClub(defaultShareClub);

		User siteAdmin1 = new User("_site", "site@custmail.com", P_ENCODER.encode("12345678"), defaultShareClub);
		User manager1 = new User("_manager", "manager@adminmail.com", P_ENCODER.encode("12345678"), defaultShareClub);
		User member1 = new User("_member", "member@ownermail.com", P_ENCODER.encode("12345678"), defaultShareClub);

		Set<Role> user1Roles = siteAdmin1.getRoles();
		Set<Role> user2Roles = manager1.getRoles();
		Set<Role> user3Roles = member1.getRoles();

		Role site = ROLE_SERV_IMPL.getRoleByName(ERole.ROLE_SITE).orElseThrow(
			() -> new RuntimeException("BS Error: Site Role not returned"));
		Role manager = ROLE_SERV_IMPL.getRoleByName(ERole.ROLE_MANAGER).orElseThrow(
			() -> new RuntimeException("BS Error: Manager Role not returned"));
		Role member = ROLE_SERV_IMPL.getRoleByName(ERole.ROLE_MEMBER).orElseThrow(
			() -> new RuntimeException("BS Error: Member Role not returned"));

		user1Roles.add(site);
		user1Roles.add(manager);
		user1Roles.add(member);
		user2Roles.add(manager);
		user2Roles.add(member);
		user3Roles.add(member);

		USER_SERV_IMPL.addSingleUser(siteAdmin1);
		USER_SERV_IMPL.addSingleUser(manager1);
		USER_SERV_IMPL.addSingleUser(member1);
	}
}

}
