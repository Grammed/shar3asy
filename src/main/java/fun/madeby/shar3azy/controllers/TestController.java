package fun.madeby.shar3azy.controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class TestController {
    @GetMapping("/all")
    public String allAccess() {
        return "Public Content.";
    }

    @GetMapping("/member")
    @PreAuthorize("hasRole('MEMBER') or hasRole('MANAGER') or hasRole('SITE')")
    public String memberAccess() {
        return "Member Board.";
    }



    @GetMapping("/manager")
    @PreAuthorize("hasRole('MANAGER') or hasRole('SITE')")
    public String managerAccess() {
        return "Manager Board.";
    }

    @GetMapping("/admin")
    @PreAuthorize("hasRole('SITE')")
    public String SiteAdminAccess() {
    return "Admin Board.";
}
}