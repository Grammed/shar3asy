package fun.madeby.shar3azy.controllers;


import fun.madeby.shar3azy.models.ERole;
import fun.madeby.shar3azy.models.Role;
import fun.madeby.shar3azy.models.ShareClub;
import fun.madeby.shar3azy.models.User;
import fun.madeby.shar3azy.payload.request.LoginRequest;
import fun.madeby.shar3azy.payload.request.SignupRequest;
import fun.madeby.shar3azy.payload.response.JwtResponse;
import fun.madeby.shar3azy.payload.response.MessageResponse;
import fun.madeby.shar3azy.repository.UserRepository;
import fun.madeby.shar3azy.security.jwt.JwtUtils;
import fun.madeby.shar3azy.security.services.UserDetailsImpl;
import fun.madeby.shar3azy.services.RoleServiceImpl;
import fun.madeby.shar3azy.services.ShareClubServiceImpl;
import fun.madeby.shar3azy.services.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/auth")
public class AuthController {
    //private final UserServiceImpl USER_SERVICE_IMPL; todo get rid @Autowired
    private final RoleServiceImpl ROLE_SERVICE_IMPL;
    private final ShareClubServiceImpl SHARECLUB_SERVICE_IMPL;
    private final AuthenticationManager AUTH_MANAGER;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        System.out.println("Sout_AuthController signin : " + loginRequest);

        Authentication authentication = AUTH_MANAGER.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        System.out.println("Sout_AuthController: singup");
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        System.out.println("Sout_AuthController: Create new");
        ShareClub shareClub = new ShareClub("AuthController passed through default name of your ShareClub");
        SHARECLUB_SERVICE_IMPL.addSingleShareClub(shareClub);

        User user = new User(signUpRequest.getUsername(),
                signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()),
                shareClub);
        System.out.println(user);


        Set<String> strRoles = signUpRequest.getRole(); // This changes a JSON array straight into a set
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = ROLE_SERVICE_IMPL.getRoleByName(ERole.ROLE_MEMBER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);

        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "site":
                        Role siteRole = ROLE_SERVICE_IMPL.getRoleByName(ERole.ROLE_SITE)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(siteRole);

                        break;
                    case "manager":
                        Role managerRole = ROLE_SERVICE_IMPL.getRoleByName(ERole.ROLE_MANAGER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(managerRole);

                        break;
                    default:
                        Role userRole = ROLE_SERVICE_IMPL.getRoleByName(ERole.ROLE_MEMBER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }
        user.setRoles(roles);
        System.out.println(roles);
        System.out.println("save user : " + user);
        userRepository.save(user);
        System.out.println("Sout_Auth Controller: save user");

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }
}