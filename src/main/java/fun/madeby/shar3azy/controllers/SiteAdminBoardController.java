package fun.madeby.shar3azy.controllers;

import fun.madeby.shar3azy.models.User;
import fun.madeby.shar3azy.services.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequiredArgsConstructor
@RequestMapping("api/test")
public class SiteAdminBoardController {
	private final UserServiceImpl USER_SERVICE_IMPL;


	@GetMapping("/list-users")
	@PreAuthorize("hasRole('SITE')")
	public List<User> allUsers() {
		return USER_SERVICE_IMPL.retrieveAllUsers();
	}
}
