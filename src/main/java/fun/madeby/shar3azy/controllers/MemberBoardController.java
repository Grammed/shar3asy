package fun.madeby.shar3azy.controllers;
// mmav1 = member manager and admin version1 == endpoints with access @ lowest non-public level.
// Endpoints appear in the first controller they are useable in so all of these are useable @ ROLE == MEMBER

import fun.madeby.shar3azy.models.PortfolioItem;
import fun.madeby.shar3azy.models.ShareClub;
import fun.madeby.shar3azy.models.Stock;
import fun.madeby.shar3azy.models.Watchlist;
import fun.madeby.shar3azy.repository.PortfolioRepository;
import fun.madeby.shar3azy.repository.WatchlistRepository;
import fun.madeby.shar3azy.security.jwt.JwtUtils;
import fun.madeby.shar3azy.services.StockServiceImpl;
import fun.madeby.shar3azy.services.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequiredArgsConstructor
@RequestMapping("api/mmav1")
public class MemberBoardController {
private final JwtUtils JWT_UTILS;
private final UserServiceImpl USER_SERVICE_IMPL;
private final StockServiceImpl STOCK_SERVICE_IMPL;
private final WatchlistRepository WATCHLIST_REPO;
private final PortfolioRepository PORTFOLIO_REPO;




//_____________<<SHARECLUB>>
// includes Watchlists and Portfolio items == universal homepage.
@GetMapping("/shareclub")
@PreAuthorize("hasRole('MEMBER') or hasRole('MANAGER') or hasRole('SITE')")
public ShareClub sharedClubInfo(HttpServletRequest request) {
	return getUserShareClub(getUserId(request));
}

//_____________<<WATCHLISTS>>
@GetMapping("/list-watchlists")
@PreAuthorize("hasRole('MEMBER') or hasRole('MANAGER') or hasRole('SITE')")
public List<Watchlist> clubsWatchLists(HttpServletRequest request) {
	return WATCHLIST_REPO.findWatchlistsByShareClubOrderByNameAsc(getUserShareClub(getUserId(request)));
}

//_____________<<PORTFOLIOITEMS>>
@GetMapping("/portfolio")
@PreAuthorize("hasRole('MEMBER') or hasRole('MANAGER') or hasRole('SITE')")
public List<PortfolioItem> clubPortfolio(HttpServletRequest request) {
	return PORTFOLIO_REPO.findPortfolioItemsByShareClub(getUserShareClub(getUserId(request)));
}

//_____________<<STOCK>>
@GetMapping("/stock")
@PreAuthorize("hasRole('MEMBER') or hasRole('MANAGER') or hasRole('SITE')")
public ResponseEntity<Map<String, Object>> getAllStocks(
	@RequestParam(required = false) String symbol,
	@RequestParam(defaultValue = "1") int page,
	@RequestParam(defaultValue = "3") int size
) {
	return STOCK_SERVICE_IMPL.retrieveAllStocksPaged(symbol, page, size);
}

@GetMapping("/stock/{searchString}")
@PreAuthorize("hasRole('MEMBER') or hasRole('MANAGER') or hasRole('SITE')")
public List<Stock> stockSymbolContains(@PathVariable String searchString) {
	return STOCK_SERVICE_IMPL.searchStringInSymbol(searchString.toUpperCase(Locale.ROOT));
}

@GetMapping("/stock/symbol/{symbolOnly}")
public Stock stockSymbolOnly(@PathVariable String symbolOnly) {
	return STOCK_SERVICE_IMPL.getStockBySymbolDotExchange(symbolOnly.toUpperCase(Locale.ROOT));
}

//_____________<<HELPER METHODS (Duplicate Problem)>>
private Long getUserId(HttpServletRequest request) {
	return JWT_UTILS.getUserIdFromReceivedRequest(request);
}
private ShareClub getUserShareClub(Long id) {
	return USER_SERVICE_IMPL.getUserById(id).getShareClub();
}
}