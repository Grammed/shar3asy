package fun.madeby.shar3azy.controllers;
// Conclusion from below == only user services when extra logic necessary.
//https://softwareengineering.stackexchange.com/questions/306890/is-it-bad-practice-that-controller-call-repository-instead-of-service
import fun.madeby.shar3azy.models.*;
import fun.madeby.shar3azy.repository.PortfolioRepository;
import fun.madeby.shar3azy.repository.WatchlistRepository;
import fun.madeby.shar3azy.security.jwt.JwtUtils;
import fun.madeby.shar3azy.services.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequiredArgsConstructor
@RequestMapping("api/test")
public class ManagerBoardController {
	private final JwtUtils JWT_UTILS;
	private final UserServiceImpl USER_SERVICE_IMPL;

	@GetMapping("/list-members")
	@PreAuthorize("hasRole('MANAGER')")
	public List<User> clubsUsers(HttpServletRequest request) {
		return USER_SERVICE_IMPL.retrieveAllUsersInClub(getUserId(request));
	}

	private Long getUserId(HttpServletRequest request) {
		return JWT_UTILS.getUserIdFromReceivedRequest(request);
	}
	private ShareClub getUserShareClub(Long id) {
		return USER_SERVICE_IMPL.getUserById(id).getShareClub();
	}




}
