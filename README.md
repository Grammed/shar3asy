# Shar3azy

Give brief synopsis

Org: Graham Duthie

By: Gram

On: 01/09/2021

Project resources used or useful

[Markdown Cheat Sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet "Adam P")

[Stack Edit](https://stackedit.io "31/08")

[Dillinger](https://dillinger.io "until you are off of visible page..")

Table of Contents:

1. [Profiles](#1)
2. [Testing](#2)

   a. [In Memory DB Bootstrap Solution](#2a)
3. [Heading 3](#3)
4. [Heading 4](#4)

<a id="1"></a>
Profiles:

<a id="2"></a>
Testing:

<a id="2a"></a>
In Memory H2 Bootstrap Solution:

*See any of the Repository tests for where this is implemented..*

Issue: WebSecurityConfig and AuthController (that needs it) were making in memory Bootstrap fail.

Solution (Ward T checkmakers):

1. @ComponentScan scanning from base package.
   1. Uses excludeFilters @ComponentScan.Filter of type ASSIGNABLE_TYPE
      1. ASSIGNABLE_TYPE == filters all classes, extended classes or implementations of specified interface.
   2. classes = {WebSecurityConfig.class, AuthController.class} are filtered.

<a id="3"></a>
Heading 3:

<a id="4"></a>
Heading 4:
    